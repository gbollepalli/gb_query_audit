import pandas as pd
import json
import pyodbc 
import base64
import os
str_1 = base64.b64decode(b'U2FpYmFiYTExMjc=')
database = 'Employee' 
username = 'gbollepalli' 
password = base64.b64decode(b'U2FpYmFiYTExMjc=')


def q():
    qu = """
        SELECT TOP 50
        Convert(varchar, qs.creation_time, 109) as Plan_Compiled_On,
        qs.last_elapsed_time as 'last_elapsed_time_by_query',
        qs.execution_count as 'Total Executions', 
        qs.total_worker_time as 'Overall CPU Time Since Compiled',
        Convert(Varchar, qs.last_execution_time, 109) as 'Last Execution Date/Time',
        cast(qs.last_worker_time as varchar) +'   ('+ cast(qs.max_worker_time as Varchar)+' Highest ever)' as 'CPU Time for Last Execution (Milliseconds)',
        Convert(varchar,(qs.last_worker_time/(1000))/(60*60)) + ' Hrs (i.e. ' + convert(varchar,(qs.last_worker_time/(1000))/60) + ' Mins & ' + convert(varchar,(qs.last_worker_time/(1000))%60) + ' Seconds)' as 'Last Execution Duration', 
        qs.last_rows as 'Rows returned',
        qs.total_logical_reads/128 as 'Overall Logical Reads (MB)', 
        qs.max_logical_reads/128 'Highest Logical Reads (MB)', 
        qs.last_logical_reads/128 'Logical Reads from Last Execution (MB)',
        qs.total_physical_reads/128 'Total Physical Reads Since Compiled (MB)', 
        qs.last_dop as 'Last DOP used',
        qs.last_physical_reads/128 'Physical Reads from Last Execution (MB)',
        t.[text] 'Query Text', 
        qp.query_plan as 'Query Execution Plan', 
        DB_Name(t.dbid) as 'Database Name', 
        t.objectid as 'Object ID', 
        t.encrypted as 'Is Query Encrypted'
        --qs.plan_handle --Uncomment this if you want query plan handle

    FROM sys.dm_exec_query_stats qs 
    CROSS APPLY sys.dm_exec_sql_text(plan_handle) AS t
    CROSS APPLY sys.dm_exec_query_plan(plan_handle) AS qp
    WHERE last_execution_time >= DATEADD(DAY, -1, GETDATE())
    ORDER BY qs.last_worker_time DESC """
    return (qu)

def to_excel():

        df1, df2, df3 = query() 
        writer = pd.ExcelWriter('query_audit.xlsx', engine='xlsxwriter')
        df1.to_excel(writer, sheet_name='som-msdb-dev-1.ucsd.edu')
        df2.to_excel(writer, sheet_name='som-msdb-qa-1.ucsd.edu')
        df3.to_excel(writer, sheet_name='som-msdb-prod-1.ucsd.edu')
        writer.save()

def query():
    queryy = q()
    servers = ["som-msdb-dev-1.ucsd.edu", "som-msdb-qa-1.ucsd.edu", "som-msdb-prod-1.ucsd.edu"]
    result = {}
    for server in servers:
        cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
        df = pd.read_sql_query(queryy, cnxn)
        result[server] = df

    return result['som-msdb-dev-1.ucsd.edu'], result['som-msdb-qa-1.ucsd.edu'], result['som-msdb-prod-1.ucsd.edu']

to_excel()
os.system('rm -r query_audit.xlsx')

query()

os.system('echo "Please Find the Query_Audit excel file. This file is for last 24 hours " | mail -s "SQL_QUERY_AUDIT" -a query_audit.xlsx  gbollepalli@health.ucsd.edu, gstrout@health.ucsd.edu, myebba@health.ucsd.edu')